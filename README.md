# The Fuzz

A tidy wrapper for [fast-check](https://github.com/dubzzz/fast-check#readme)

We add ES6 importing:
```javascript
// These all work:
const fuzz = require('the-fuzz');
const { fuzz } = require('the-fuzz');
import fuzz from 'the-fuzz';
import { fuzz } from 'the-fuzz';
```

Seeds set on the CLI are available when using `fuzz` instead of fc.assert:
```javascript
node ./myProgram --seed=23456789
```
To not use the CLI seed, either override it:
```javascript
fuzz(/* Your Fuzzing */, { seed: 12 })
```
Or use the original assert:
```javascript
fuzz.assert(/* Your Fuzzing */);
```

All fc properties available on the fuzz object:
```javascript
fuzz.property
fuzz.string
fuzz.assert
...
```