
const fc = require('fast-check');

const seedParam = "--seed=";

let seed = process.argv.filter(a => a.indexOf(seedParam) === 0)[0];

if(seed) {
  seed = seed.slice(seedParam.length);
  seed = Number(seed);
} else {
  // Generate seed if none given
  // Taken from fast-check's own default: https://github.com/dubzzz/fast-check/blob/469f52cb6236e658c5d4a3cac4545972227486a8/src/check/runner/configuration/QualifiedParameters.ts#L28
  seed = Date.now() ^ (Math.random() * 0x100000000);
}

// Create a fuzz function
function fuzz() {
  const args = Array.prototype.slice.call(arguments);
  args.push({seed: seed});
  return fc.assert.apply(null, args);
};

// Assign the fc object onto fuzz, and allow it to behave like a default import
module.exports = Object.assign(fuzz, fc, {default: fuzz, _default: fuzz, fuzz: fuzz});